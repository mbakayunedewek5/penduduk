<h4>List Data</h4>

<table>
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>Alamat</th>
		</tr>
	</thead>
	<tbody>
		<?php if (!empty($list)): ?>
			<?php $no = $this->uri->segment('3') + 1; ?>
			<?php foreach ($list as $key => $value): ?>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $value->nama ?></td>
					<td><?php echo $value->alamat ?></td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>

<br/>
<?php echo $this->pagination->create_links(); ?>