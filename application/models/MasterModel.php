<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterModel extends CI_Model
{
	function getdata($where=null, $limit=null, $offset=null)
	{
		if (!empty($where)) {
			$this->db->where($where);
		}

		$this->db->order_by('nama', 'ASC');

		if (!empty($limit)) {
			return $this->db->get('penduduk', 10, $offset);
		}
		else {
			return $this->db->get('penduduk');
		}
	}
}