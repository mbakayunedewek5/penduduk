<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
  		parent::__construct();

  		$this->load->library('template');
  	}

	public function index() {
		$this->template->show('home');
	}

	public function data() {
  		$this->load->model('MasterModel', 'model');
		
		$this->load->library('pagination');

		$jumlah_data = $this->model->getdata()->num_rows();
		$limit = 10;

		$config['base_url'] 	= base_url().'home/data/';
		$config['total_rows'] 	= $jumlah_data;
		$config['per_page'] 	= $limit;

		$offset = $this->uri->segment(3);

		$this->pagination->initialize($config);

		$data['list'] = $this->model->getdata(null, $limit, $offset)->result();

		$this->template->show('data', $data);
	}

	public function add() {
		$this->template->show('form');
	}

}
