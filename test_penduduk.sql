-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2018 at 01:49 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test_penduduk`
--

-- --------------------------------------------------------

--
-- Table structure for table `penduduk`
--

DROP TABLE IF EXISTS `penduduk`;
CREATE TABLE IF NOT EXISTS `penduduk` (
`id` int(11) NOT NULL,
  `nik` char(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` text
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `penduduk`
--

TRUNCATE TABLE `penduduk`;
--
-- Dumping data for table `penduduk`
--

INSERT INTO `penduduk` (`id`, `nik`, `nama`, `alamat`) VALUES
(1, '3404111807670002', 'ZULKARNAN', 'Jalan Raya 234 Bantul Yogyakarta'),
(2, '3404114508630002', 'HJ. JUMARIAH', 'Jalan Raya 234 Bantul Yogyakarta'),
(3, '3404115906940002', 'NAURA LATIFAH UMAMI', 'Jalan Raya 234 Bantul Yogyakarta'),
(4, '3404111906640001', 'MUH. ZUKHRI', 'Jalan Raya 234 Bantul Yogyakarta'),
(5, '3404122808800004', 'MUSTOFA', 'Jalan Raya 234 Bantul Yogyakarta'),
(6, '3404124308810008', 'DIAH PANGASTUTI PRIHANDINI', 'Jalan Raya 234 Bantul Yogyakarta'),
(7, '3404111410090002', 'MAHYA AIDIN MUSTAFA', 'Jalan Raya 234 Bantul Yogyakarta'),
(8, '3404111204130001', 'MAHEERA ATHALLA MUSTAFA', 'Jalan Raya 234 Bantul Yogyakarta'),
(9, '3302262507920001', 'RIFKI FESTIAWAN', 'Jalan Raya 234 Bantul Yogyakarta'),
(10, '3404119504700002', 'MUH SUPARDI', 'Jalan Raya 234 Bantul Yogyakarta'),
(11, '3319085004800001', 'NIKMATUN', 'Jalan Raya 234 Bantul Yogyakarta'),
(12, '3404115304350001', 'NAUFAL AZIZAH RAIS', 'Jalan Raya 234 Bantul Yogyakarta'),
(13, '3404115304350001', 'JUMINAH', 'Jalan Raya 234 Bantul Yogyakarta'),
(14, '3404110202630001', 'MARYONO', 'Jalan Raya 234 Bantul Yogyakarta'),
(15, '3404115907630001', 'HARYANI', 'Jalan Raya 234 Bantul Yogyakarta'),
(16, '3404112010980001', 'IMAM YUHDI', 'Jalan Raya 234 Bantul Yogyakarta'),
(17, '3322060508620006', 'MOCH. WAHYUDI', 'Jalan Raya 234 Bantul Yogyakarta'),
(18, '3322066203720001', 'DOKTER. DRG. JUNI HANDAJANI', 'Jalan Raya 234 Bantul Yogyakarta'),
(19, '3322052511820002', 'ADIYASA WAHYU NURIYANTO', 'Jalan Raya 234 Bantul Yogyakarta'),
(20, '3322062708970002', 'SATRIO WAHYU PRATAMA', 'Jalan Raya 234 Bantul Yogyakarta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `penduduk`
--
ALTER TABLE `penduduk`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `penduduk`
--
ALTER TABLE `penduduk`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=118;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
